# SNN - Estudo de caso
Template para estudo de viabilidade de desenvolvimento do backend SNN.

A ideia é servir como o core do backend que será resposável pelas regras de negócios e acesso ao banco de dados. Outros recursos como disparo de emails e notificações, deve-se fazer uso de micro serviços externos, sendo usado apenas como ponte.

## Recursos implementados

* AdonisJS 4.1
* ACL - https://github.com/enniel/adonis-acl / https://www.npmjs.com/package/adonis-acl / https://www.npmjs.com/package/adonis-acl-advanced
* Docker
* GraphQL
* MySQL
* Testes unitários

### Recursos a implementar
* SoftDelete - https://github.com/radmen/adonis-lucid-soft-deletes

## Execução local

Renomear o arquivo **.env.example** para **.env**

Instalar o Adonis CLI

    npm i -g @adonisjs/cli

Rodar o comando
    
    docker-compose up mysql
    docker exec -it snn-mysql bash
    mysql -u root -p123456
    create database snn;
Sair do docker e rodar o comando
    
    adonis migration:run

Rodar a api com o comando

    node server.js

Importar o arquivo **docs/Insomnia.json**

## Modelo Entidade x Relacionamento
![modelo err](./docs/err.png)

## Exemplo de requisições
Dentro da pasta **docs**, há um arquivo que deve ser importado no insomnia
![modelo err](./docs/insomnia.png)

## Material para estudo

**Debugging AdonisJS with vscode**
https://viglucci.io/debugging-adonis-with-vscode

**AdonisJs. Named Middlewares with passing props**
https://medium.com/@artem.diashkin/adonisjs-how-to-implement-named-middleware-with-passing-props-d4d9629a984c

**Adonis Repository Pattern**
https://github.com/waqaradil/AdonisRepo

**Build a GraphQL Server With Apollo Server and AdonisJS**
https://scotch.io/tutorials/build-a-graphql-server-with-apollo-server-and-adonisjs

**Effortless Real-time GraphQL API with serverless business logic running in any cloud**
https://medium.com/open-graphql/effortless-real-time-graphql-api-with-serverless-business-logic-running-in-any-cloud-8585e4ed6fa3

## Dúvidas

O que devo fazer com o SoftDelete caso um registro filho (Course > User), no caso deste registro sofrer "soft delete" o que deve ocorrer? deve ser apresentado, OU deve ser omitido no retorno dos dados?

## CHECAR

    module.exports = {
        lucidConfig: require('./lucidConfiguration'),
        createTables: require('./createTables'),
        dropTables: require('./dropTables')
    }
    *************************
    'use strict'

        /**
        * adonis-acl
        * Copyright(c) 2017 Evgeny Razumov
        * MIT Licensed
        */

        const Model = use('Adonis/Src/Model')

        class Permission extends Model {
        static get rules () {
            return {
            slug: 'required|min:3|max:255|regex:^[a-zA-Z0-9_-]+$',
            name: 'required|min:3|max:255',
            description: 'min:3|max:1000'
            }
        }
        }

        module.exports = Permission

