#!/bin/bash

echo "NPM install"
npm install

echo "Copy .env file:"
file="./.env.docker"
if [ -f "$file" ]
then
	echo "$file found."
	cp $file ./.env
	echo ".env created"
else
	echo "$file not found."
	exit 1
fi

echo "Run migration"
adonis migration:run --force

echo "Start node server"
adonis serve --dev --polling
