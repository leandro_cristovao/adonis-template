'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const GraphqlAdonis = use('ApolloServer')
const schema = use('App/GraphQL/schema');

Route.get('/', () => {
  return { greeting: 'Estudo de caso para backend do SNN' }
})

Route.group(() => {
  Route.post('login', 'UserController.login')
  //todo: apenas para fins de testes
  Route.resource('base', 'BaseController')
  Route.resource('city', 'CityController').only(['index', 'show'])
}).middleware('guest')

Route.group(() => {
  Route.post('register', 'UserController.register')
  Route.resource('user', 'UserController').apiOnly()
  Route.resource('course', 'CourseController').apiOnly()
}).middleware('auth')

Route.group(() => {
  Route.resource('admin/tenant', 'Admin/TenantController').apiOnly()
}).middleware(['auth', 'adminAccess'])

// **** GRAPHQL ****
Route.route('/graphql', ({ request, auth, response }) => {
  return GraphqlAdonis.graphql({
    schema,
    context: { auth }
  }, request, response)
}, ['GET', 'POST'])
  .middleware('auth')