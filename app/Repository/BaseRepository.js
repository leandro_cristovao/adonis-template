'use strict'

const { adjustParameter } = require('../GraphQL/utils')
const Database = use('Database')

class BaseRepository {

  model
  constructor(model) {
    this.model = model
  }

  async create(data, auth, trx) {
    return await this.model.create(data, trx)
  }

  async merge(data, auth, trx) {
    let entity = await this.find(data.id, auth)
    if (!entity)
      throw new Error("Entidade não encontrada!")

    entity.merge(data)
    await entity.save()
    return entity
  }

  async all(auth) {
    return await this.model.all()
  }

  async find(id, auth) {
    return await this.model.find(id) 
  }

  async paginate(params) {
    params.searchParam = adjustParameter(params.searchParam)

    let query = this.model.query()

    if (params.searchParam.filters)
      params.searchParam.filters.map(f => {
        query.where(f.field, f.operator, f.value)
      })

    query = await query.paginate(params.searchParam.pagination.page, params.searchParam.pagination.perPage)
    return query
  }
}

module.exports = BaseRepository