'use strict'

const { adjustParameter } = require('../GraphQL/utils')
const BaseRepository = use('App/Repository/BaseRepository')
const Database = use('Database')

class BaseTenantRepository extends BaseRepository {

  async create(data, auth) {
    data.user_id = auth.user.id
    data.tenant_id = auth.user.tenant_id
    return await this.model.create(data)
  }

  async create(data, auth, trx) {
    data.user_id = auth.user.id
    data.tenant_id = auth.user.tenant_id
    return await this.model.create(data, trx)
  }

  async merge(data, auth, trx) {
    let entity = await this.find(data.id, auth)
    if (!entity)
      throw new Error("Entidade não encontrada!")

    entity.merge(data)
    await entity.save()
    return entity
  }

  async all(auth) {
    let query = this.model.query()
    if (auth.user.tenant_id != 1)
      query.where('tenant_id', auth.user.tenant_id)
    return await query.fetch()
  }

  async find(id, auth) {
    let query = this.model.query()

    // if (params.searchParam && params.searchParam.showDeleted && params.searchParam.showDeleted == true)
    //   query = query.withTrashed()

    query.where('id', id)
    query = query.withTrashed()

    if (auth.user.tenant_id != 1)
      query.where('tenant_id', auth.user.tenant_id)

    let data = await query.fetch()

    return data.rows ? data.rows[0] : null
  }

  async paginate(params, auth) {
    params.searchParam = adjustParameter(params.searchParam)

    let query = this.model.query()
    if (params.searchParam && params.searchParam.showDeleted && params.searchParam.showDeleted == true)
      query = query.withTrashed()

    if (auth.user.tenant_id != 1)
      query.where('tenant_id', auth.user.tenant_id)

    if (params.searchParam.filters)
      params.searchParam.filters.map(f => {
        query.where(f.field, f.operator, f.value)
      })

    query = await query.paginate(params.searchParam.pagination.page, params.searchParam.pagination.perPage)
    return query
  }

}

module.exports = BaseTenantRepository