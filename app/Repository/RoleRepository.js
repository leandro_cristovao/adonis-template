'use strict'

const BaseRepository = use('App/Repository/BaseRepository')
const Role = use('Role')
class RoleRepository extends BaseRepository {
  constructor() {
    super(use('Role'))
  }

  async create(data, auth, trx) {
    let permissions = data.permissions
    delete data.permissions

    //todo leandro: nao funciona com transaction
    // let entity = await this.model.create(data, trx)
    // await entity.save(trx)

    // if (permissions) {
    //   await entity.permissions().attach(permissions, trx)
    // }

    let roleID = await trx.insert(data).into('roles')
    permissions = permissions.map(p => {
      return {
        role_id: roleID,
        permission_id: p
      }
    })
    
    await trx.insert(permissions).into('permission_role')
    await trx.commit()

    let entity = await this.find(roleID[0], auth)
    return entity
  }

  async merge(data, auth, trx) {
    let entity = await this.find(data.id, auth)
    if (!entity)
      throw new Error("Entidade não encontrada!")

    let permissions = data.permissions
    if (permissions) {
      await entity.permissions().attach(permissions)
    }
    delete data.permissions

    entity.merge(data, trx)
    await entity.save(trx)
    return entity
  }

}

module.exports = RoleRepository