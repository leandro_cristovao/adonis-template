'use strict'

const BaseRepository = use('App/Repository/BaseRepository')

class TenantRepository extends BaseRepository {
  userRepository = {}

  constructor() {
    super(use('App/Models/Tenant'))
    this.userRepository = new BaseRepository(use('App/Models/User'))
  }

  async create(data, auth, trx) {
    data.user_id = auth.user.id
    let tenant = await this.model.create({ company: data.company, user_id: 1 }, trx)

    data.user.tenant_id = tenant.id
    data.user.user_id = auth.user.id

    const user = await this.userRepository.create(data.user, auth, trx)
    tenant.user_id = user.id

    await tenant.save(trx)
    
    return tenant
  }

  async merge(data, auth, trx) {
    
    let entity = await this.find(data.id, auth)
    if (!entity)
      throw new Error("Entidade não encontrada!")

    entity.merge({ company: data.company }, trx)
    await entity.save()
    
    return entity
  }
}

module.exports = TenantRepository