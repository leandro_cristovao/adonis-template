'use strict'

const BaseTenantRepository = use('App/Repository/BaseTenantRepository')

class UserRepository extends BaseTenantRepository {
  constructor() {
    super(use('App/Models/User'))
  }

  async login(email, password, auth) {
    try {
      const token = await auth.attempt(email, password)
      const user = await this.model.findBy('email', email)

      return {
        status: 'success',
        data: token,
        user
      }
    } catch (error) {
      return {
        status: 'error',
        message: error.message || JSON.stringify(error)
      }
    }
  }

  async register(data, auth) {
    data.tenant_id = auth.user.tenant_id
    data.user_id = auth.user.id
    const user = await this.model.create(data, auth)
    return user
  }

  async create(data, auth, trx) {
    let { permissions, roles } = data

    delete data.permissions
    delete data.roles

    data.user_id = auth.user.id
    data.tenant_id = auth.user.tenant_id
    return await this.model.create(data, trx)
  }

  async merge(data, auth, trx) {
    debugger
    let { permissions, roles } = data
    delete data.permissions
    delete data.roles

    let entity = await this.find(data.id, auth)
    if (!entity)
      throw new Error("Entidade não encontrada!")

    if (permissions) {
      await entity.permissions().attach(permissions)
    }

    if (roles) {
      await entity.roles().attach(roles)
    }

    entity.merge(data, trx)
    await entity.save(trx)
    return entity
  }

}

module.exports = UserRepository