'use strict'

const resolvers = {
  ...use('App/GraphQL/Resolver'),
  Query: { ...use('App/GraphQL/Query') },
  Mutation: { ...use('App/GraphQL/Mutation') }
}

module.exports = resolvers