const City = require('./City')
const Course = require('./Course')
const Permission = require('./Permission')
const Role = require('./Role')
const Tenant = require('./Tenant')
const User = require('./User')

module.exports = {
  City,
  Course,
  Permission,
  Role,
  Tenant,
  User,
}