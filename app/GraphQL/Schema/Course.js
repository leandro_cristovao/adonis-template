'use strict'

class Course {

  static get graphQLType() {
    return `   
    type Course {
      id: Int!
      deleted: Boolean!
      name: String!
      user: User!
    }

    type CourseData {
      pages: PaginationData!
      rows: [Course]
    }

    input CourseInput {
      id: Int
      name: String!
    }
    `
  }

  static get graphQLQuery() {
    return `
    Course(
      searchParam: SearchParam
    ):CourseData
    `
  }

  static get graphQLMutation() {
    return `
    upsertCourse(input: CourseInput): Course
    `
  }
}

module.exports = Course
