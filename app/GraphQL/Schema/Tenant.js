'use strict'



class Tenant {

  static get graphQLType() {
    return `   
    type Tenant {
      id: Int!
      company: String!
      deleted: Boolean!
      user: User!
    }

    type TenantData {
      pages: PaginationData!
      rows: [Tenant]
    }

    input TenantInput {
      id: Int
      company: String!
      user: UserInput!
    }
    `
  }

  static get graphQLQuery() {
    return `
    Tenant(
      searchParam: SearchParam
    ):TenantData
    `
  }

  static get graphQLMutation() {
    return `
    upsertTenant(input: TenantInput): Tenant
    `
  }
}

module.exports = Tenant
