'use strict'

class Role {


  static get graphQLType() {
    return `   
    type Role {
      id: Int!
      description: String
      name: String!
      slug: String!
      permissions: [Permission]
    }

    type RoleData {
      pages: PaginationData!
      rows: [Role]
    }

    input RoleInput {
      id: Int
      slug: String!
      name: String!
      description: String
      permissions: [Int!]
    }
    `
  }

  static get graphQLQuery() {
    return `
    Role(
      searchParam: SearchParam
    ):RoleData
    `
  }

  static get graphQLMutation() {
    return `
    upsertRole(input: RoleInput): Role
    `
  }
}

module.exports = Role
