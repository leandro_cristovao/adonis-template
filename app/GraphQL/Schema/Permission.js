'use strict'

class Permission {

  static get graphQLType() {
    return `   
    type Permission {
      id: Int!
      description: String
      name: String!
      slug: String!
    }

    type PermissionData {
      pages: PaginationData!
      rows: [Permission]
    }

    input PermissionInput {
      id: Int
      slug: String!
      name: String!
      description: String
    }
    `
  }

  static get graphQLQuery() {
    return `
    Permission(
      searchParam: SearchParam
    ):PermissionData
    `
  }

  static get graphQLMutation() {
    return `
    upsertPermission(input: PermissionInput): Permission
    `
  }
}

module.exports = Permission
