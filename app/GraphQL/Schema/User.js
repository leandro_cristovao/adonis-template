'use strict'

class User {

  static get graphQLType() {
    return `   
    type User {
      id: Int!
      deleted: Boolean!
      email: String!
      tenant: Tenant
      username: String!

      permissions: [Permission]
      roles: [Role]
    }

    type UserData {
      pages: PaginationData!
      rows: [User]
    }

    type Login {
      status: String!
      message: String
      data: LoginToken
      user: User
    }

    type LoginToken {
      type: String!
      token: String!
    }

    input UserInput {
      id: Int
      username: String!
      email: String!
      password: String!
      permissions: [Int!]
      roles: [Int!]
    }
    `
  }

  static get graphQLQuery() {
    return `
    User(
      searchParam: SearchParam
    ):UserData
    `
  }

  static get graphQLMutation() {
    return `
    login(email: String!, password: String!): Login
    upsertUser(input: UserInput): User
    `
  }
}

module.exports = User