const UserRepository = use('App/Repository/UserRepository')
let userRepository = null

module.exports = {
  loadUser: async (id, auth) => {
    if (!userRepository) {
      userRepository = new UserRepository()
    }

    return await userRepository.find(id, auth)
  }
}