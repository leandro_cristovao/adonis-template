const Course = require('./course')
const Role = require('./role')
const Tenant = require('./tenant')
const User = require('./user')
module.exports = {
  Course,
  Role,
  Tenant,
  User
}