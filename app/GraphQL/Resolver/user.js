const Role = use('Role')
const Permission = use('Permission')
const User = use('App/Models/User')
const Tenant = use('App/Models/Tenant')

module.exports = {
  deleted: (entity) => entity.deleted_at != null,

  permissions: async (entity, params, { auth }) => {
    //todo leandro: devo implementar  a paginacao padrao?
    const user = await User.find(entity.id)
    const permissions = await user.permissions().fetch()
    return permissions.rows
  },

  roles: async (entity, params, { auth }) => {
    //todo leandro: devo implementar  a paginacao padrao?
    const user = await User.find(entity.id)
    const roles = await user.roles().fetch()
    return roles.rows
  },

  tenant: async (entity, params, { auth }) => {
    const tenant = await Tenant.find(entity.tenant_id)
    return tenant
  }
}