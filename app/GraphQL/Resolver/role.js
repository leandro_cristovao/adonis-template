const Role = use('Role')

module.exports = {
  permissions: async (entity, params, { auth }) => {
    //todo leandro: devo implementar  a paginacao padrao?
    const role = await Role.find(entity.id)
    const permissions = await role.permissions().fetch()
    return permissions.rows
  }
}