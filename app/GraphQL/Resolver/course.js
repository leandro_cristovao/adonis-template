const { loadUser } = use('./utils')

module.exports = {
  user: async (entity, params, { auth }) => {
    return await loadUser(entity.user_id, auth)
  },
  deleted: (entity) => entity.deleted_at != null
}