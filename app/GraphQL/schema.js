'use strict'

const { makeExecutableSchema } = require('graphql-tools')
const resolvers = require('./resolvers')

const {
  Course,
  Permission,
  Role,
  Tenant,
  User,
} = use('App/GraphQL/Schema')

const typeDefs = `
  input Pagination {
    page: Int!
    perPage: Int!
  }
  
  input Filter {
    field: String!
    operator: String!
    value: String!
  }
  
  input SearchParam {
    filters: [Filter]
    pagination: Pagination
    showDeleted: Boolean    
  }
  
  type PaginationData {
    total: Int
    perPage: Int
    page: Int
    lastPage: Int
  }
  
  ${Course.graphQLType}
  ${Permission.graphQLType}
  ${Role.graphQLType}
  ${Tenant.graphQLType}
  ${User.graphQLType}
  
  type Query {
    ${Course.graphQLQuery}
    ${Permission.graphQLQuery}
    ${Role.graphQLQuery}
    ${Tenant.graphQLQuery}
    ${User.graphQLQuery}
  }
  
  type Mutation {
    deleteModel(model: String!, id: Int!): Int!
    ${Course.graphQLMutation}
    ${Permission.graphQLMutation}
    ${Role.graphQLMutation}
    ${Tenant.graphQLMutation}
    ${User.graphQLMutation}
  }
`

module.exports = makeExecutableSchema({ typeDefs, resolvers })