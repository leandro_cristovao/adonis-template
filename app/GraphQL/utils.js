const adjustParameter = (params) => {
  params.searchParam = params.searchParam || {}
  params.searchParam.filters = params.searchParam.filters || []

  params.searchParam.pagination = params.searchParam.pagination || {}
  params.searchParam.pagination.page = params.searchParam.pagination.page || 1
  params.searchParam.pagination.perPage = params.searchParam.pagination.perPage || 5

  return params
}

const loadEntities = async (Entity, params) => {
  let entities = Entity.query()

  if (params.searchParam.filters)
    params.searchParam.filters.map(f => {
      entities.where(f.field, f.operator, f.value)
    })

  entities = await entities.paginate(params.searchParam.pagination.page, params.searchParam.pagination.perPage)
  return entities
}

module.exports = {
  adjustParameter,
  loadEntities
}