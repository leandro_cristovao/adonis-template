'use strict'

const UserRepository = use('App/Repository/UserRepository')
let repository = null

module.exports = {
  async login(_, { email, password }, { auth }) {
    if (!repository) {
      repository = new UserRepository()
    }
    return await repository.login(email, password, auth)
  },

  async upsertUser(_, { input }, { auth }) {
    if (!repository) {
      repository = new UserRepository()
    }

    if (!input.id)
      return await repository.create(input, auth)
    return await repository.merge(input, auth)
  }
}