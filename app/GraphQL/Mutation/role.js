const Database = use('Database')
const RoleRepository = use('App/Repository/RoleRepository')
let repository = null

module.exports = {
  async upsertRole(_, { input }, { auth }) {

    if (!repository) {
      repository = new RoleRepository()
    }

    const trx = await Database.beginTransaction()
    try {

      let entity = {}
      if (!input.id)
        entity = await repository.create(input, auth, trx)
      else
        entity = await repository.merge(input, auth, trx)

      await trx.commit()
      return entity
    } catch (ex) {
      await trx.rollback()
      throw new Error(ex.message)
    }

  }
}