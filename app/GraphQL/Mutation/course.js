const BaseTenantRepository = use('App/Repository/BaseTenantRepository')
let repository = null

module.exports = {
  async upsertCourse(_, { input }, { auth }) {

    if (!repository) {
      repository = new BaseTenantRepository(use('App/Models/Course'))
    }
    
    if (!input.id)
      return await repository.create(input, auth)
    return await repository.merge(input, auth)
  }
}