const BaseRepository = use('App/Repository/BaseRepository')
let repository = null

module.exports = {
  async upsertPermission(_, { input }, { auth }) {

    if (!repository) {
      repository = new BaseRepository(use('Permission'))
    }
    
    if (!input.id)
      return await repository.create(input, auth)
    return await repository.merge(input, auth)
  }
}