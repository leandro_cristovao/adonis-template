const { login, upsertUser } = use('App/GraphQL/Mutation/user')
const { upsertCourse } = use('App/GraphQL/Mutation/course')
const { upsertPermission } = use('App/GraphQL/Mutation/permission')
const { upsertRole } = use('App/GraphQL/Mutation/role')
const { upsertTenant } = use('App/GraphQL/Mutation/tenant')

const fs = require('fs')

const deleteModel = async (_, { model, id }, { auth }) => {
  try {
    let Repository = null
    let repository = {}

    const path = '../../Repository'
    let fileName = `${path}/${model}Repository.js`

    let Entity = use('App/Models/' + model)
    
    //Preciso checar se existe um repositorio específico e chama-lo
    if (fs.existsSync(fileName)) {
      Repository = require(fileName)
      repository = new Repository()
    }

    if (!Repository) {
      Repository = require('../../Repository/BaseTenantRepository')
      repository = new Repository(Entity)
    }

    let obj = await repository.find(id, auth)

    //objeto nao existe
    if (!obj)
      return 0

    await obj.delete()
    return obj.id
  } catch (ex) {
    if (ex.code = 'MODULE_NOT_FOUND')
      throw new Error(`Model não encontrado: ${model}`)
    throw ex
  }
}

module.exports = {
  deleteModel,
  login,
  upsertCourse,
  upsertPermission,
  upsertRole,
  upsertTenant,
  upsertUser,
}