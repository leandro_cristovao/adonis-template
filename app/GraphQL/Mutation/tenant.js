const Database = use('Database')
const TenantRepository = use('App/Repository/TenantRepository')
let repository = null

module.exports = {
  async upsertTenant(_, { input }, { auth }) {

    if (!repository) {
      repository = new TenantRepository()
    }
    
    const trx = await Database.beginTransaction()
    try {
      
      let entity = {}
      if (!input.id)
        entity = await repository.create(input, auth, trx)
      else
        entity = await repository.merge(input, auth, trx)

      await trx.commit()
      return entity
    } catch (ex) {
      await trx.rollback()
      throw new Error(ex.message)
    }
  }
}