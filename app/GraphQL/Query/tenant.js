const BaseRepository = use('App/Repository/BaseRepository')
const Tenant = use('App/Models/Tenant')

let repository = null
module.exports = async function (_, searchParams, { auth }) {
  if (!repository) {
    repository = new BaseRepository(Tenant)
  }

  return await repository.paginate(searchParams, auth)
}