const BaseRepository = use('App/Repository/BaseRepository')
const Role = use('Role')

let repository = null
module.exports = async function (_, searchParams, { auth }) {
  if (!repository) {
    repository = new BaseRepository(Role)
  }

  return await repository.paginate(searchParams, auth)
}