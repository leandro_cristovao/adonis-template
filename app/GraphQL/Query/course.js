const BaseTenantRepository = use('App/Repository/BaseTenantRepository')
const Course = use('App/Models/Course')

let repository = null
module.exports = async function (_, searchParams, { auth }) {
  if (!repository) {
    repository = new BaseTenantRepository(Course)
  }

  return await repository.paginate(searchParams, auth)
}