const BaseTenantRepository = use('App/Repository/BaseTenantRepository')
const User = use('App/Models/User')

let repository = null
module.exports = async function (_, searchParams, { auth }) {
  if (!repository) {
    repository = new BaseTenantRepository(User)
  }

  return await repository.paginate(searchParams, auth)
}