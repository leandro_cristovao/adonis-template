const BaseRepository = use('App/Repository/BaseRepository')
const Permission = use('Permission')

let repository = null
module.exports = async function (_, searchParams, { auth }) {
  if (!repository) {
    repository = new BaseRepository(Permission)
  }

  return await repository.paginate(searchParams, auth)
}