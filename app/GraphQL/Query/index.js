const Course = require('./course')
const Permission = require('./permission')
const Role = require('./role')
const Tenant = require('./tenant')
const User = require('./user')

module.exports = {
  Course,
  Permission,
  Role,
  Tenant,
  User
}