'use strict'

const Response = require('@adonisjs/framework/src/Response')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class AdminAccess {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ auth, response }, next) {
    await next()
    if (Number.parseInt(auth.user.id) != 1) {
      return response.status(403).json({
        message: 'Somente usuário master da SNN pode acessar conteúdo',
        code: 'E_INVALID_PERMISSION',
        status: 403
      })
    }

  }
}

module.exports = AdminAccess
