'use strict'

const User = use('App/Models/User')
const BaseTenantController = use('App/Controllers/BaseTenantController')

class UserController extends BaseTenantController {

  constructor() {
    super(User)
  }

  async login({ auth, request, response }) {
    try {
 

      const { email, password } = request.all()
      const token = await auth.attempt(email, password)

      let user = await User.findBy('email', email)

      return response.json({
        status: 'success',
        data: token,
        user
      })

    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message || JSON.stringify(error)
      })
    }
  }

  async register({ request, auth, response }) {
    const data = request.only(['name', 'username', 'email', 'password'])

    try {
      const user = await this.model.create(data, auth)
      const token = await auth.generate(user)

      return response.json({
        status: 'success',
        data: token,
        user
      })
    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message || JSON.stringify(error)
      })
    }
  }
}

module.exports = UserController