'use strict'

const BaseController = use('App/Controllers/BaseController')
const City = use('App/Models/City')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with base
 */
class CityController extends BaseController {

  constructor() {
    super(City)
  }
}

module.exports = CityController
