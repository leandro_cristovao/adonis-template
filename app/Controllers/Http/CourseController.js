'use strict'

const BaseTenantController = use('App/Controllers/BaseTenantController')
const Course = use('App/Models/Course')

class CourseController extends BaseTenantController {

  constructor() {
    super(Course)
  }
}

module.exports = CourseController