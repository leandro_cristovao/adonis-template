'use strict'

const BaseController = use('App/Controllers/BaseController')
const BaseRepository = use("App/Repository/BaseRepository")
const Database = use('Database')
const Tenant = use('App/Models/Tenant')
const User = use('App/Models/User')

class TenantController extends BaseController {

  userRepository = {}
  constructor() {
    super(Tenant)
    this.userRepository = new BaseRepository(User)
  }

  async store({ request, response, auth }) {
    const trx = await Database.beginTransaction() 
    const dataTenant = request.only(['company'])
    let dataUser = request.post().user

    try {
      let tenant = await this.model.create(dataTenant, auth, trx)

      dataUser.tenant_id = tenant.id
      dataUser.user_id = auth.user.id
      const user = await this.userRepository.create(dataUser, auth, trx)
      tenant.user = user

      await trx.commit()
      return response.json({
        status: 'success',
        data: tenant
      })
    } catch (error) {
      await trx.rollback()
      return response.status(400).json({
        status: 'error',
        message: error.message || JSON.stringify(error)
      })
    }
  }
}

module.exports = TenantController
