'use strict'

const BaseRepository = use('App/Repository/BaseRepository')
const Database = use('Database')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with base
 */
class BaseController {

  model = {}
  constructor(modelReference) {
    this.model = new BaseRepository(modelReference)
  }
  /**
   * Show a list of all base.
   * GET base
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ response, auth }) {
    try {

      let data = await this.model.all(auth)

      return response.json({
        status: 'success',
        data
      })

    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message || JSON.stringify(error)
      })
    }
  }

  /**
   * Render a form to be used for creating a new object.
   * GET base/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ response }) {
    return response.status(400).json({
      status: 'error',
      message: 'GET: create method not found'
    })
  }

  /**
   * Create/save a new object.
   * POST base
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.all()

    try {
      const trx = await Database.beginTransaction()
      const obj = await this.model.create(data, trx)
      await trx.commit()

      return response.json({
        status: 'success',
        data: obj
      })
    } catch (error) {
      await trx.rollback()
      return response.status(400).json({
        status: 'error',
        message: error.message || JSON.stringify(error)
      })
    }
  }

  /**
   * Display a single object.
   * GET base/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, response, auth }) {
    try {

      let data = await this.model.find(params.id, auth)

      return response.json({
        status: 'success',
        data
      })

    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message || JSON.stringify(error)
      })
    }
  }

  /**
   * Render a form to update an existing object.
   * GET base/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ response }) {
    return response.status(400).json({
      status: 'error',
      message: 'GET: base/:id/edit method not found'
    })
  }

  /**
   * Update object details.
   * PUT or PATCH base/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ response }) {
    return response.status(400).json({
      status: 'error',
      message: 'PUT/PATCH: update method not found'
    })
  }

  /**
   * Delete a object with id.
   * DELETE base/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ response }) {
    return response.status(400).json({
      status: 'error',
      message: 'DELETE: base/:id method not found'
    })
  }
}

module.exports = BaseController
