'use strict'

const BaseTenantRepository = use('App/Repository/BaseTenantRepository')
const BaseController = use('App/Controllers/BaseController')
const Database = use('Database')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with base
 */
class BaseTenantController extends BaseController {
  constructor(modelReference) {
    super()
    this.model = new BaseTenantRepository(modelReference)
  }

  async store({ request, response, auth }) {
    const data = request.all()

    const trx = await Database.beginTransaction()
    try {
      const obj = await this.model.create(data, auth, trx)
      await trx.commit()

      return response.json({
        status: 'success',
        data: obj
      })
    } catch (error) {
      await trx.rollback()
      return response.status(400).json({
        status: 'error',
        message: error.message || JSON.stringify(error)
      })
    }
  }
}

module.exports = BaseTenantController