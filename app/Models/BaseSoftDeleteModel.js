const BaseModel = require('./BaseModel')

class BaseSoftDeleteModel extends BaseModel {
  static boot() {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
}

module.exports = BaseSoftDeleteModel