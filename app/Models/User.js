'use strict'

const BaseSoftDeleteModel = require('./BaseSoftDeleteModel')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends BaseSoftDeleteModel {
  static boot() {
    super.boot()

    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  static get traits() {
    return [
      '@provider:Adonis/Acl/HasRole',
      '@provider:Adonis/Acl/HasPermission'
    ]
  }

  static get hidden() {
    return ['password']
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }

  /** GRAPHQL */
  static get graphQLType() {
    return `   
    type User {
      id: Int!
      username: String!
      email: String!
      permissions: [Permission]
      roles: [Role]
      tenant: Tenant
    }

    type UserData {
      pages: PaginationData!
      rows: [User]
    }

    type Login {
      status: String!
      message: String
      data: LoginToken
      user: User
    }

    type LoginToken {
      type: String!
      token: String!
    }

    input UserInput {
      id: Int
      username: String!
      email: String!
      password: String!
      permissions: [Int!]
      roles: [Int!]
    }
    `
  }

  static get graphQLQuery() {
    return `
    User(
      searchParam: SearchParam
    ):UserData
    `
  }

  static get graphQLMutation() {
    return `
    login(email: String!, password: String!): Login
    upsertUser(input: UserInput): User
    `
  }
}

module.exports = User