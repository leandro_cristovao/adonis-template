const City = require('./City')
const Course = require('./Course')
const Tenant = require('./Tenant')
const Token = require('./Token')
const User = require('./User')

module.exports = {
  City,
  Course,
  Tenant,
  Token,
  User,
}