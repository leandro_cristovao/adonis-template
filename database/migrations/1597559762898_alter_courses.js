'use strict'

const Schema = use('Schema')

class CoursesTableSchema extends Schema {
  up() {
    this.alter('courses', table => {
      table.dateTime('deleted_at')
    })
  }

  down() {
    this.alter('courses', table => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = CoursesTableSchema
