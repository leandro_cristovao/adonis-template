'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CourseSchema extends Schema {
  up () {
    this.createIfNotExists('courses', (table) => {
      table.increments()
      table.timestamps()
      table.integer('tenant_id').unsigned()
      table.foreign('tenant_id').references('tenants.id')
      table.string('name', 255).notNullable().unique()
      table.integer('user_id').unsigned().references('id').inTable('users')
    })
  }

  down () {
    this.dropExtensionIfExists('courses')
  }
}

module.exports = CourseSchema
