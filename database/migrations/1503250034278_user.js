'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Database = use('Database')
const Hash = use('Hash')
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.createIfNotExists('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.integer('tenant_id').unsigned().references('id').inTable('tenants').notNullable()
      table.integer('user_id').unsigned().references('id').inTable('users').notNullable()
      table.timestamps()
    })

    this.schedule(async (trx) => {
      await Database.table('users').transacting(trx).insert({
        username: 'admin',
        email: 'admin@snn.com',
        tenant_id: 1,
        user_id: 1,
        password: await Hash.make('123456')
      })
    })
  }

  down () {
    this.dropExtensionIfExists('users')
  }
}

module.exports = UserSchema
