'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Database = use('Database')
const Hash = use('Hash')
const Schema = use('Schema')

class TenantSchema extends Schema {
  up() {
    this.alter('tenants', (table) => {
      table.integer('user_id').unsigned().references('id').inTable('users').notNullable().alter()
    })
  }

  down() {
    this.alter('tenants', (table) => {
      table.integer('user_id').unsigned().alter()
    })
  }
}

module.exports = TenantSchema
