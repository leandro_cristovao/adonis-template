'use strict'

const Database = use('Database')
const Schema = use('Schema')

class InsertRoleSchema extends Schema {
  up () {
    this.schedule(async (trx) => {
      /**
       * Não mudar a ordem para não prejudicar os IDs
       */
      await Database.table('roles').transacting(trx).insert([
        { slug: 'administrador', name: 'Administrador', description: 'Usuário administrador do tenant' }
      ])

      await Database.table('permissions').transacting(trx).insert([
        { slug: 'criar_usuario', name: 'Criar usuário', description: 'Criar usuário' }
      ])

      await Database.table('permission_role').transacting(trx).insert([
        { permission_id: 1, role_id: 1 }
      ])      
    })
  }

  down () {
    
  }
}

module.exports = InsertRoleSchema
