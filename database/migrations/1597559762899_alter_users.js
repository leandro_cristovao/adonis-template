'use strict'

const Schema = use('Schema')

class UsersTableSchema extends Schema {
  up() {
    this.alter('users', table => {
      table.dateTime('deleted_at')
    })
  }

  down() {
    this.alter('users', table => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = UsersTableSchema
