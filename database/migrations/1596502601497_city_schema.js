'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Database = use('Database')
const Schema = use('Schema')

class CitySchema extends Schema {
  up() {
    this.createIfNotExists('cities', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 255).notNullable()
      table.string('state', 2).notNullable()
    })

    this.schedule(async (trx) => {
      await Database.table('cities').transacting(trx).insert([
        { name: 'Rio de Janeiro', state: 'RJ' },
        { name: 'São José', state: 'SC' },
        { name: 'Florianópolis', state: 'SC' }
      ])
    })
  }

  down() {
    this.dropExtensionIfExists('cities')
  }
}

module.exports = CitySchema
