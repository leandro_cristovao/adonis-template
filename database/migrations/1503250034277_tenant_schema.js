'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Database = use('Database')
const Schema = use('Schema')

class TenantSchema extends Schema {
  up () {
    this.createIfNotExists('tenants', (table) => {
      table.string('company', 255).notNullable().unique()
      table.integer('user_id').unsigned()
      table.increments()
      table.timestamps()      
    })

    this.schedule(async (trx) => {
      await Database.table('tenants').transacting(trx).insert({company: 'SENAR nas nuvens', user_id: 1 })
    })
  }

  down () {
    this.dropExtensionIfExists('tenants')
  }
}

module.exports = TenantSchema
